package graphTest;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

import org.junit.Test;
import graph.BasicGraphBFSImpl;
import org.junit.Before;

public class BasicGraphBFSImplTest {
    BasicGraphBFSImpl graph = new BasicGraphBFSImpl();

	@Before
	public void setup() {
		String data = "6\r\nfalse\r\n10\r\n15\r\n12\r\n24\r\n23\r\n-1\r\n";
		InputStream in = System.in;
		Scanner scanner = null;
		try {
	        System.out.println("Enter the number of vertices:");	        
			
			System.setIn(new ByteArrayInputStream(data.getBytes()));
			scanner = new Scanner(System.in);
	        graph.setVertices(Integer.parseInt(scanner.nextLine()));

	        System.out.println("Is it directed graph? ");
	        boolean directed = Boolean.parseBoolean(scanner.nextLine());

	        graph.buildAdjacencyMatrix(scanner, directed);
		}
		finally {
			System.setIn(in);
			scanner.close();
		}
	}
	
	@Test
	public void testBuildAdjacencyMatrix() {
        int arr[][] = {{0,1,0,0,0,0}, {1,0,1,0,0,1}, {0,1,0,1,1,0}, {0,0,1,0,0,0}, {0,0,1,0,0,0}, {0,1,0,0,0,0}};
        assertArrayEquals(null, arr, graph.getGraph());
        graph.printGraph();
	}
	
	@Test
	public void testBfsIterative() {
		graph.bfsIterative(0);
		int[] arr = {1,2,5,3,4,-1};
		assertTrue("Arrays are not same", Arrays.equals(arr, graph.getRoute()));
	}

	static public void assertArrayEquals(String message,
            int[][] expected,
            int[][] actual) {

		// If both arrays are null, then we consider they are equal
		if (expected == null && actual == null) {
			return;
		}

		// We test to see if the first dimension is the same.
		if (expected.length != actual.length) {
			fail(message + ". The array lengths of the first dimensions aren't the same.");
		}

		// We test every array inside the 'outer' array.
		for (int i=0; i< expected.length; i++) {
			assertTrue(message + ". Array no." + i + " in expected and actual aren't the same.",
					Arrays.equals(expected[i], actual[i]));
		}
	}

}
