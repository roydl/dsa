package queues;

import stacks.EmptyStackException;
import stacks.FullStackException;

public class BasicQueue {
	static final int QUEUE_CAPACITY = 10;
	int capacity;
	int front;
	int rear;
	
	int[] queue;
	
	public BasicQueue(){
		this(QUEUE_CAPACITY);
	}
	
	public BasicQueue(int capacity) {
		this.capacity = capacity;
		queue = new int[capacity];
	}
	
	public int size() {
		return capacity - front + rear; // capacity - (rear - front)
	}
	
	public boolean isEmpty() {
		return rear == front;
	}
	
	public void enqueue (int element) throws FullStackException {
		if (size() == capacity - 1) {
			throw new FullStackException("Queue is full");
		}
		queue[rear++] = element;
	}
	
	public int dequeue() throws EmptyStackException {
		if (isEmpty()) {
			throw new EmptyStackException("Queue is empty");
		}
		int tmp = queue[front];
		queue[front++] = -1;
		return tmp;
	}

}
