package queues;

class BasicStack {
	final static int CAPACITY_STACK = 100;
	int top = -1;
	int capacity;
	Object[] _stack;

	public BasicStack() {
		this(CAPACITY_STACK);
	}
	
	public BasicStack(int capacity){
		this.capacity = capacity;
		_stack = new Object[capacity];
	}
	
	public int size() {
		return top + 1;
	}

	public boolean isEmpty() {
		return (top < 0);
 	}

	public Object peek() throws Exception {
		if (isEmpty()) {
			throw new Exception("Stack is empty");
		}
		
		return _stack[top];
	}

	public Object pop() throws Exception {
		if (isEmpty()) {
			throw new Exception("Stack is empty");
		}
		
		Object tmp = _stack[top];
		_stack[top--] = null;
 		return tmp;
	}

	public void push(Object element) throws Exception {
		if (size() == capacity){
			throw new Exception("Stack is full");
		}
		_stack[++top] = element;
	}
}

public class QueueUsingStack {
	BasicStack stack1;
	BasicStack stack2;
	
	QueueUsingStack(){
		stack1 = new BasicStack();
		stack2 = new BasicStack();
	}
	
	public void enqueue(int data) {
		try {
			stack1.push(data);
		} 
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public int dequeue(){
		if ( stack1.isEmpty() && stack2.isEmpty()) {
			System.out.println("Empty queue!");
			return -1;
		}

		try {
			if (stack2.isEmpty()) {
				while(! stack1.isEmpty()){
					stack2.push(stack1.pop());
				}
			}
			return (int) stack2.pop();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return -1;
	}
	
	public static void main(String[] args){
		QueueUsingStack queue = new QueueUsingStack();
		queue.enqueue(10);
		queue.enqueue(20);
		queue.enqueue(30);
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());
		queue.enqueue(40);
		queue.enqueue(50);
		queue.enqueue(60);
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());
		System.out.println("Dequeuing..." + queue.dequeue());

	}
}
