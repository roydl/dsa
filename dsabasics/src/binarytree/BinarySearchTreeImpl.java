package binarytree;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTreeImpl {
    Node root;
    
    private Node insert( Node node, int data ) {
        Node local = new Node(data);
        if ( node == null ) {
            node = local;
            return node;
        }
        
        if ( data < node.getData() ) {
            node.setLeft( insert(node.getLeft(), data));
        } else {
            node.setRight( insert(node.getRight(), data));
        }
        return node;
    }
    
    public void insertRecursive( int data ) {
        root = insert(root, data);
    }
    
    public Node insertNonRecursive ( int data ) {
        Node node = new Node(data);
        if ( root == null ) {
            root = node;
            return root;
        }

        Node current = root;
        while ( true ) {
            if ( data < current.getData() ) {
                if ( current.getLeft() != null ) {
                    current = current.getLeft();
                }
                else {
                    current.setLeft(node);
                    break;
                }
            }
            else {
                if ( current.getRight() != null ) {
                    current = current.getRight();
                }
                else {
                    current.setRight(node);
                    break;
                }
            }
        }
        
        return current;
    }
    
    public boolean recursiveSearch(Node node, int data) {
    	if (node == null) {
    		return false;
    	}
    	
		if ( data < node.getData() ) {
			return recursiveSearch( node.getLeft(), data);
		}
		else if ( data > node.getData() ) {
			return recursiveSearch( node.getRight(), data);
		}
		else {
			return true;
		}
    }
    
    public boolean search(int data) {
    	if (root == null) {
    		return false;
    	}
    	
    	Node current = root;
    	while( current != null ) {
    		if ( data < current.getData() ) {
    			current = current.getLeft();
    		}
    		else if ( data > current.getData() ) {
    			current = current.getRight();
    		}
    		else {
    			return true;
    		}
    	}
    	return false;
    }
    
    public boolean isBst(Node node)
    {
        return isBinarySearchTree(node , Integer.MIN_VALUE , Integer.MAX_VALUE);
    }
    
    private boolean isBinarySearchTree(Node node , int min , int max)
    {
    	// Empty tree is BST
    	if (node == null ) {
    		return true;
    	}

    	// false if this node violates the min / max constraints
        if( node.getData() < min || node.getData() > max) {
        	return false;
        }
        
        return (isBinarySearchTree(node.getLeft(), min, node.getData() - 1) && isBinarySearchTree(node.getRight(), node.getData() + 1, max));
    }

    
    public boolean printBSTWithinRange(Node node, int min, int max)
    {
    	// Empty tree is BST
    	if (node == null ) {
    		return true;
    	}

    	// false if this node violates the min / max constraints
        if( node.getData() > min && node.getData() < max) {
        	System.out.print(node.getData() + " ,");
        }
        
        return (printBSTWithinRange(node.getLeft(), min, node.getData() - 1) && printBSTWithinRange(node.getRight(), node.getData() + 1, max));
    }

    
    
    public boolean isBST(Node node) {
    	if ( node == null ) {
    		return false;
    	}
    	
    	System.out.println("Checking for data " + node.getData());
    	if (node.getLeft() != null ) {
    		if ( node.getLeft().getData() > node.getData()) {
    			return false;
    		}
    	}
    	
    	if (node.getRight() != null ) {
    		if ( node.getRight().getData() < node.getData()) {
    			return false;
    		}
    	}
    	
    	if ( ! isBST(node.getLeft()) || ! isBST(node.getRight()))
    		return false;
    	return true;
    }

    public Node getRoot() {
        return root;
    }
    
    public void setRoot(Node node) {
        root = node;    
    }
    
    // always presents data in sorted order
    public void inOrderTraversal(Node node) {
        if ( node == null ) {
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getData() + " ");
        inOrderTraversal(node.getRight());
    }

    public int findHeight(Node node) {
        if ( node == null || ( node.getLeft() == null && node.getRight() == null  )) {
            return 0;
        }
        
        int leftHeight = findHeight(node.getLeft());
        int rightHeight = findHeight(node.getRight());

        return  (leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1);
    }
    
    
	public void levelOrderTraversal ( ) {
		if ( root == null ) {
			System.out.println("BST is empty");
			return;
		}
	        
		Queue<Node> queue = new LinkedList<Node>();
	        queue.add(root);
	        
	        while (! queue.isEmpty() ) {
	            Node node = queue.poll();
	            System.out.println(node.getData() + ", ");
	            
	        	if ( node.getLeft() != null ) {
	        	    queue.add(node.getLeft());
	        	}
	        	
	        	if ( node.getRight() != null ) {
	        	    queue.add(node.getRight());
	        	}
	        }        
	}
    
	

    
    public static void main(String[] args) {
        BinarySearchTreeImpl tree = new BinarySearchTreeImpl();
        tree.insertNonRecursive(30);
        tree.insertNonRecursive(38);
        tree.insertNonRecursive(12);
        tree.insertNonRecursive(14);

        System.out.print("InOrderTraversal: ");
        tree.inOrderTraversal(tree.getRoot());
        System.out.println(" ");

        System.out.println("Can find 31 " + tree.recursiveSearch(tree.getRoot(), 31));
        System.out.println("Can find 12 " + tree.recursiveSearch(tree.getRoot(), 12));
        System.out.println("Can find 14 " + tree.recursiveSearch(tree.getRoot(), 14));
        System.out.println("Can find 38 " + tree.recursiveSearch(tree.getRoot(), 38));
        System.out.println("Can find 39 " + tree.recursiveSearch(tree.getRoot(), 39));
        System.out.println("Can find 30 " + tree.recursiveSearch(tree.getRoot(), 30));

        System.out.println("Can find 31 - " + tree.search(31));
        System.out.println("Can find 12 - " + tree.search(12));
        System.out.println("Can find 14 - " + tree.search(14));
        System.out.println("Can find 38 - " + tree.search(38));
        System.out.println("Can find 39 - " + tree.search(39));
        System.out.println("Can find 30 - " + tree.search(30));

        System.out.println("Tree is BST - " + tree.isBst(tree.getRoot()));
        //tree.printBSTWithinRange(tree.getRoot(), 13, 35);
        tree.levelOrderTraversal();
    }
}