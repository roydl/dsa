package graph;

public class DijkstraAlgorithm 
{ 
  private static final int V=6; 

  int minDistance(int key[], Boolean visited[]) 
  { 
      int min = Integer.MAX_VALUE, min_index=-1; 
      for (int v = 0; v < V; v++) {
    	  if (visited[v] == false && key[v] < min) { 
              min = key[v]; 
              min_index = v; 
          } 
      }

      return min_index; 
  } 

  void printMST(int parent[], int n, int graph[][], int[] dist) 
  { 
      System.out.println("Edge \tWeight \t Distance"); 
      for (int i = 1; i < V; i++) {
    	  System.out.println(parent[i]+" - "+ i+"\t"+ 
    			  graph[i][parent[i]] + "\t " + dist[i]); 
      }

      printShortestPath(parent);
  } 
 
  final char[] array = { 'A', 'B', 'C', 'D', 'E', 'F'};

  public void printPath( int current, int[] parent) {
	  if ( current == -1 ) {
		  return;
	  }
	  
	  printPath(parent[current], parent);
	  System.out.print(array[current] + " --> ");
  }
  
  public void printShortestPath (int[] parent) {
	  for (int i = 0; i < V-1; i++)  {
		  System.out.println("Shortest Path to reach Node: " + array[i] + " is: ");
		  printPath( parent[i], parent);
		  System.out.println( array[i]);
	  }
  }

  void dijkstraMST(int graph[][]) 
  { 
      int parent[] = new int[V]; 
      int dist[] = new int [V]; 
      Boolean visited[] = new Boolean[V]; 

      for (int i = 0; i < V; i++) { 
          dist[i] = Integer.MAX_VALUE; 
          visited[i] = false; 
      } 

      dist[0] = 0;  
      parent[0] = -1; 

      for (int count = 0; count < V-1; count++) { 
          int u = minDistance(dist, visited); 
          visited[u] = true; 

          for (int v = 0; v < V; v++) {
              if (graph[u][v] !=0 && visited[v] == false && 
                  dist[u] + graph[u][v] < dist[v]) 
              { 
                  dist[v] = graph[u][v] + dist[u]; 
                  parent[v] = u; 
              } 
          }
      } 

      printMST(parent, V, graph, dist); 
  } 
  
  /*
   *           3
   *        B ---- C
   *   10 / |    /   \ 11
   *     A  | 7 / 8   D
   *   25 \ |  /    / 16
   *        F ---- E
   *            2
   */
  
  public static void main (String[] args) 
  { 
      DijkstraAlgorithm t = new DijkstraAlgorithm(); 
      int graph[][] = new int[][] {{0, 10, 0, 0, 0, 25}, 
                                  {10, 0, 3, 0, 0, 7}, 
                                  {0, 3, 0, 11, 9, 8}, 
                                  {0, 0, 11, 0, 16, 0}, 
                                  {0, 0, 9, 16, 0, 2},
                                  {25, 7, 8, 0, 2, 0}}; 

      t.dijkstraMST(graph); 
  } 
}