package graph;
import java.util.Scanner;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class TopologicalSortImpl {
    int[][] matrix;
    int[] route;
    int[] visitedNodes;
    
    Stack<Integer> startNodes = new Stack<Integer>();
    List<Integer> result = new ArrayList<Integer>();
    
    int loopCount;
    int vertices;
    static TopologicalSortImpl graph;    
    
    public void setVertices(int vertices) {
        this.vertices = vertices;    
    }

    public int[][] getGraph() {
    	return matrix;
    }
    
    // only for testing
    public int[] getRoute() {
    	return route;
    }
    
    public int getLoopCount() {
    	return loopCount;
    }
    
    public void buildAdjacencyMatrix(Scanner sc, boolean directed) {
        matrix = new int[vertices][vertices];
        route = new int[vertices];
        visitedNodes = new int[vertices];
        
        int i = 0, j = 0;
        while (sc.hasNextInt()){
            int usrInput = sc.nextInt();
            if ( usrInput == -1) {
                break;
            }
            
            i = usrInput / 10;
            j = usrInput % 10;

            if ( i < vertices && j < vertices ) {
                if ( directed ) {
                    matrix[i][j] = 1;
                }
                else {
                    matrix[i][j] = 1;
                    matrix[j][i] = 1;
                }
            }
        }        
    } 

    public void printGraph() {
        System.out.println("The adjacency matrix is: ");

        for (int x=0; x < vertices; x++) {
            for(int y =0; y < vertices; y++) {
                System.out.print(matrix[x][y] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void setupGraph() {
		String data = "6\r\ntrue\r\n01\r\n21\r\n30\r\n34\r\n45\r\n52\r\n-1\r\n";
		InputStream in = System.in;
		Scanner scanner = null;
		try {
	        System.out.println("Enter the number of vertices:");	        
			
			System.setIn(new ByteArrayInputStream(data.getBytes()));
			scanner = new Scanner(System.in);
	        graph.setVertices(Integer.parseInt(scanner.nextLine()));

	        System.out.println("Is it directed graph? ");
	        boolean directed = Boolean.parseBoolean(scanner.nextLine());

	        graph.buildAdjacencyMatrix(scanner, directed);
		}
		finally {
			System.setIn(in);
			scanner.close();
		}
	}
	
	public void findStartNode () {
        int start = -1;
	    for (int y=0; y < vertices; y++) {
	        for (int x = 0; x < vertices; x++) {
    	        if ( matrix[x][y] == 0 ) {
    	            start = y;
    	        }
    	        else {
    	            start = -1;
    	            break;
    	        }
	        }
	        if ( start != -1 ) {
	            startNodes.push(start);
	            start = -1;
	        }
	    }
	    return;
	}

    public int[][] cloneArray() {
        int length = matrix.length;
        int[][] target = new int[length][matrix[0].length];
        for (int i = 0; i < length; i++) {
            System.arraycopy(matrix[i], 0, target[i], 0, matrix[i].length);
        }
        return target;
    }	
    
    public void topologicalSort ( int[][] srcMatrix ) {
        Set<Integer> visited = new HashSet<Integer>();
        
        while (! startNodes.isEmpty()) {
            int start = startNodes.pop();
            if (! visited.contains(start)) {
                visited.add(start);
                result.add(start);
            }
        
            for ( int i=0; i < vertices; i++) {
                if ( srcMatrix[start][i] == 1) {
                    if (! visited.contains(i)) {
                        result.add(i);
                        visited.add(i);
                    }
                    srcMatrix[start][i] = 0;
                
                    // if i'th column does not have any incoming connection, 
                    // then add it to the startNodes
                    boolean iHasIncomingNode = false;
                    for (int x= 0; x < vertices; x++ ) {
                        if (srcMatrix[x][i] != 0 ) {
                            iHasIncomingNode = true;
                            break;
                        }                        
                    }
                    
                    if (! iHasIncomingNode) {
                        startNodes.add(i);
                    } 
                }
            }
        }
        
        char[] alpha = new char[vertices];
        for(int i = 0; i < vertices; i++) {
            alpha[i] = (char)(65 + i);
        }
        
        System.out.println("Topological Sort order is: ");
        for (int i : result) {
        	System.out.print(" " + alpha[i]);
        }
    }
	
	public static void main(String args[]) {
        graph = new TopologicalSortImpl();
        setupGraph();
        graph.printGraph();
        
        graph.findStartNode();
        
        if ( graph.startNodes.isEmpty() ) {
            System.out.println("No start node found");
            System.exit(-1);
        }
        
        for ( int startNode : graph.startNodes) {
            System.out.println("Starting node found and the node number is : " + startNode);
        }
        
        int[][] matrixCopy = graph.cloneArray();
        graph.topologicalSort(matrixCopy);
	}
}
