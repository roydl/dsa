package linkedlist;

class Node {
	private int _data;
	private Node _next;
	
	Node(int data) {
		_data = data;
		_next = null;
	}
	
	public Node getNext() {
		return _next;
	}
	
	public void setNext( Node node ) {
		_next = node;
	}
	
	public int getData() {
		return _data;
	}
	
	public void setData( int data ) {
		_data = data;
	}
}

public class LinkedListImpl {
	private Node head;
	
	private void insertInFront( int data, Node current) {
		Node node = new Node( data );
		node.setNext(current);
		head = node;
	}
	
	private void insertAt( int data, Node prev, Node next) {
		Node node = new Node( data );
		prev.setNext(node);
		node.setNext(next);
	}

	public void insertLast ( int data, Node current ) {
		Node node = new Node( data );
		current.setNext(node);
	}

	/*
	 * Insert elements in linked list in sorted order
	 */
	public void insertSorted ( int data ) {
		if ( head == null ) {
			head = new Node(data);
			return;
		}

		Node current = head;
		if ( data <= current.getData() ) {
			insertInFront (data, current);
			return;
		}
		
		Node prev = head;
		while ( current != null ) {
			if ( data <= current.getData() ) {
				insertAt (data, prev, current);
				return;
			}
			else {
				prev = current;
				current = current.getNext();
				continue;
			}
		}
		
		insertLast(data, prev);
	}

	/*
	 * Count the number of nodes in a linked list using recursion
	 */
	public int nodeCount( Node current ) {
		if ( current == null ) {
			return 0;
		}
		else {
			return 1 + nodeCount ( current.getNext() );
		}
	}

	/*
	 * Reverse a linked list
	 */
	public Node reverseLL(Node _head) {
	    Node curr = _head;
	    Node prev = null;
	    Node next = null;
	    
	    while ( curr != null ) {
	        next = curr.getNext();
	        curr.setNext(prev);
	        prev = curr;
	        curr = next;
	    }
	    return prev;
	}
	
	/*
	 * Find mid point
	 */
	public void findMiddle() {
	    Node fast = head;
	    Node slow = head;
	    
	    if ( head == null ) {
	        return;
	    }
	    
	    while ( fast != null && fast.getNext() != null ){
	        slow = slow.getNext();
	        fast = fast.getNext().getNext();
	    }
	    
	    System.out.println("The middle element is : " + slow.getData());
	}

	
	/*
	 * Nodes to be counted from 1
	 * Perform list traversal twice
	 * Mark the list via first traversal between m and n
	 * reverse the section between m and n
	 */
	public Node reverseRange(int m, int n) {
		Node start = null;
		Node end = null;
		Node prev = null;
		Node nextOfn = null;
		
		Node curr = head;
		int i = 1;
		while (curr != null ) {
			if ( i < m ) {
				prev = curr;
			}
			
			if ( i == m ) {
				start = curr;
			}
			
			if ( i == n ) {
				end = curr;
				nextOfn = curr.getNext();
			}
			
			curr = curr.getNext();
			i++;
		}
		
		end.setNext(null);
		
		prev.setNext(reverseLL(start));
		
		start.setNext(nextOfn);
		
		return head;
	}
	
	public static void main(String[] args) {
		LinkedListImpl ll = new LinkedListImpl();
		ll.insertSorted(10);
		ll.insertSorted(10);
		ll.insertSorted(20);
		ll.insertSorted(5);
		ll.insertSorted(15);
		
		Node current = ll.head;
		while ( current != null ) {
			System.out.print(current.getData() + ", ");
			current = current.getNext();
		}
		
		System.out.println();
		
		System.out.println("The node count in the list is: " + ll.nodeCount(ll.head));
		
        ll.head = ll.reverseLL(ll.head);
        current = ll.head;
        System.out.println("Reversed linked list contents are: ");
		while ( current != null ) {
			System.out.print(current.getData() + ", ");
			current = current.getNext();
		}
		
		
		LinkedListImpl ll1 = new LinkedListImpl();
		ll1.insertSorted(5);
		ll1.insertSorted(10);
		ll1.insertSorted(15);
		ll1.insertSorted(20);
		ll1.insertSorted(45);
		ll1.insertSorted(50);
		ll1.insertSorted(70);
		
		ll1.reverseRange(2, 5);
        
		current = ll1.head;
        System.out.println("\nReversed linked list contents from m to n are: ");

		while ( current != null ) {
			System.out.print(current.getData() + ", ");
			current = current.getNext();
		}

	}
}
