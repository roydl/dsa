package recursion;

public class EqualSumGroups {
	
	public static int findSplitPoint (int[] arr){
		int leftSum = 0;
		for ( int i=0; i < arr.length; i++) {
			leftSum += arr[i];
			
			int rightSum = 0;
			
			for ( int j=i+1; j < arr.length; j++){
				rightSum += arr[j];
			}
			
			if ( leftSum == rightSum ) {
				return i + 1;
			}
		}
		return -1;
	}
	
	public static void printTwoParts (int[] arr, int splitPoint) {
        if (splitPoint == -1 || splitPoint == arr.length )
        {
            System.out.println("Not Possible");
            return;
        }

		for (int i = 0; i < arr.length; i++)
        {
            if(splitPoint == i)
               System.out.println();
                
            System.out.print(arr[i] + " ");
        }
	}
	

	public static void main(String[] args) {
		int[] array = {4, 2,3, 9};
		int splitsPoint = findSplitPoint(array);
		printTwoParts( array, splitsPoint);
	}
}
