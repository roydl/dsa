package recursion;

/*
 * Find the nth number of Fibonacci series (1,1,2,3,5,8,13,21...)
 * using recursion
 * Note: fib series nth item is sum of n-1 and n-2th items
 */

public class Fibonacci {
	
	public static int fib ( int num ) {
		if ( num == 1 ) {
			return 1;
		}
		else if ( num == 2 ) {
			return 1;
		}
		else {
			return fib( num - 1 ) + fib( num - 2 );
		}
	}
	
	public static void main( String[] args) {
		System.out.println("Fibonacci number at 4th position is: " + fib(5));
		System.out.println("Fibonacci number at 2nd position is: " + fib(2));
		System.out.println("Fibonacci number at 1st position is: " + fib(6));


		
	}
}
