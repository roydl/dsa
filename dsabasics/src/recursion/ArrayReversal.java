package recursion;

public class ArrayReversal {

	public static void reverseArray( int[] array, int[] revArr, int len, int idx ) {
		if ( len - 1 < 0 ) {
			return;
		}
		else {
			revArr[idx] = array[len -1];
			reverseArray( array, revArr, --len, ++idx );
		}
	}
	

	
	public static void printArr(int[] reverseArr) {
		System.out.print("Reverse array element is: {");
		for ( int i : reverseArr ) {
			System.out.print( i + ", ");
		}
		System.out.println("}");
	}
	
	public static void main( String[] args ){
		int[] arr = { 2, 3, 4, 5, 8};
		int[] reverseArr = new int[arr.length];
		reverseArray( arr, reverseArr, arr.length, 0);
		printArr(reverseArr);
		
		int[] arr1 = { };
		int[] revA1 = new int[arr1.length];
		
		reverseArray( arr1, revA1, arr1.length, 0);
		printArr(revA1);
	}
}
