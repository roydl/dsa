package recursion;

public class ShrinkString {
	static int n = 0;
    
    public static String shrink( String s, String t ) {
        if ( s.length() == 0 ) {
            return t;
        }
        
        if ( t == null ) {
            t = s.charAt(0) + "";
        }
        else if ( ! t.endsWith(s.charAt(0)+"")) {
            t = t + s.charAt(0);
        }
        return shrink(s.substring(1), t);
    }
    
    public static String shrink( String s ) {    	
    	if ( n + 1 >= s.length()) {
    		n = 0;
    		return s;
    	}
    	
    	if ( s.charAt(n) == s.charAt(n+1)) {
    		return shrink(s.substring(0,n) + s.substring(n+1));
    	}
    	else {
    		n++;
    		return shrink(s);
    	}
    }    
    
    public static void main( String[] args ){
        String a = "abbbbcf";
        System.out.println("Shrinked string for:" + a +   " is :" + shrink(a));
        
        String b = "abbbbaaccccfffgf";
        System.out.println("Shrinked string for:" + b +   " is :" + shrink(b, null));
        System.out.println("Shrinked string for:" + b +   " is :" + shrink(b));
        
        String c = "abc";
        System.out.println("Shrinked string for: " + c + "  is " + shrink(c));
    }
}