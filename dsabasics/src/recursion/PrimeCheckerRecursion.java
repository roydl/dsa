package recursion;

public class PrimeCheckerRecursion {
	private static int i = Integer.MAX_VALUE;

	public static boolean isPrime(int num, int i) {
		i = ( i > num ) ? num : i;
		
		if ( i == 1 ) {
			return true;
		}
		else if ( num % i == 0 && num != i) {
				return false;
		}
		else
			return isPrime( num, --i);
	}
	
	public static boolean isPrime( int num ) {
		i = ( i > num) ? num : i;
		
		if ( i == 1 || i == 0 ) {
			i = Integer.MAX_VALUE;
			return true;
		}
		else if ( num % i == 0 && num != i) {
			i= Integer.MAX_VALUE;
			return false;
		}
		else {
			i--;
			return isPrime( num );
		}
	}
	
	public static void main ( String[] args ) {
		System.out.println("3 is prime:" + isPrime(3, 3));
		System.out.println("Number 6 is prime:" + isPrime(6, 6));
		System.out.println("Number 7 is prime:" + isPrime(7, 9));
		
		System.out.println("3 is prime using second method:" + isPrime( 3 ));
		System.out.println("Number 6 is prime using second method:" + isPrime( 6 ));
		System.out.println("Number 7 is prime using second method:" + isPrime( 7 ));
		System.out.println("Number 9 is prime using second method:" + isPrime( 9 ));


	}

}
