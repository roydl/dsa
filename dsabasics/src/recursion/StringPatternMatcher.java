package recursion;

public class StringPatternMatcher {
	
	static String[] array = new String[10];
	static int n = 0;
	
	public static int findSubString ( String s, String pat) {
		if ( s.length() < pat.length() ) {
			return 0;
		}
		else {
			return s.indexOf(pat);
		}
	}
	
	public static int strDist(String str, String sub) {
	    if (sub.isEmpty()) {
	        throw new IllegalArgumentException("sub mustn’t be empty");
	    }
	    if (str.length() <= sub.length()) {
	        if (str.equals(sub)) {
	            return str.length();
	        } else { // sub cannot be in str
	            return 0;
	        }
	    }
	    if (str.startsWith(sub)) {
	        if (str.endsWith(sub)) {
	            return str.length();
	        } else {
	            return strDist(str.substring(0, str.length() - 1), sub);
	        }
	    } else {
	        return strDist(str.substring(1), sub);
	    }
	}
		
	public static int findMaxSubstring ( String s, String pat) {
		if ( s.length() < pat.length() ) {
			return 0;
		}
		else if ( s.startsWith(pat, 0)){
			int idx = findSubString( s.substring(pat.length()), pat );
			if ( idx == -1 || idx == 0 ) {
				return -1;
			}
			array[n++] = s.substring(pat.length(), pat.length() + idx);
			return findMaxSubstring( s.substring(pat.length()), pat);
		}
		else {
			return 1 + findMaxSubstring( s.substring(1), pat);
		}		
	}
	
	public static int indexOf(String str, String sub, int start, int direction) {
		  if (start < 0 || start > str.length() - sub.length())
		    return -1;
		  if (str.substring(start, start + sub.length()).equals(sub))
		    return start;
		  return indexOf(str, sub, start+direction, direction);
		}

		public static int strDistRecursive(String str, String sub) {
		  int first = indexOf(str, sub, 0, 1);
		  if (first == -1)
		    return 0;
		  int last = indexOf(str, sub, str.length() - sub.length(), -1);
		  return last - first + sub.length();
		}
	
	public static void main(String[] args) {
		String s = "catwomencatwhiskerscat";
		System.out.println("Count using net method is : " + strDistRecursive(s, "cat"));
		System.out.println("Count is : " + findMaxSubstring( s, "cat"));
		for ( String str : array) {
			if (str != null )
				System.out.println("The string is:" + str + " and it's len is: " + str.length());
		}
	}

}
