package recursion;

class PrefixEval {
    static int pos = 0;
    
    public static int evalPrefix( String os ) {
		String[] stringArray = os.split(" ");

    	String operator = os.substring(pos);
		char ch = operator.charAt(0);
    	
        if( Character.isDigit(ch)) {
            return Character.getNumericValue(ch);
        } else if (ch == '*' || ch == '/' || ch == '+' || ch == '=') {
        	pos = pos + 1;
        	int op1 = evalPrefix(os);
        	pos = pos + 1;
            int op2 = evalPrefix(os);
            
            System.out.println("Operator is:" + ch + " op1 is: " + op1 + " op2 is:" + op2);
            
            // Normal calculation
            switch (ch) {
            case '*':
            	return op1 * op2;
            case '/':
            	return op1 / op2;
            case '+':
            	return op1 + op2;
            case '-':
            	return op1 - op2;
            }
        }
        return -1;
    }
    
    public static void main( String args[]) {
    	String s1 = "+/82*42";
        int ans = evalPrefix(s1);
        System.out.println("Prefix operator evaluation for:" + s1 + " is:" + ans);
    }
}