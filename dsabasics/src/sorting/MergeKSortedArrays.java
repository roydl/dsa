package sorting;

/*
* There are k-sorted arrays. Together they have N values. They wanted to get one sorted array of 10 elements.
* approach this without using heap sort.
 */
public class MergeKSortedArrays {

    public static void merge(int[] left, int[] right, int[] array) {
        int nL = left.length;
        int nR = right.length;
        int i=0, j =0 , k = 0;
        while( i < nL && j < nR) {
            if ( left[i] <= right[j]){
                array[k] = left[i++];
            }
            else {
                array[k] = right[j++];
            }
            k++;
        }

        while (i < nL){
            array[k++] = left[i++];
        }

        while (j < nR){
            array[k++] = right[j++];
        }
    }


    public static void main(String[] args) {
        int[] arr1 = {3,6,7,9,10};
        int[] arr2 = {1,2,4,5,8};
        int[] arrR = new int[10];

        merge(arr1, arr2, arrR);

        for (int i : arrR) {
            System.out.print(" " + i);
        }
    }

}
