package sorting;

public class MergeSort {

	public void merge ( int[] array, int l, int m, int r) {
		int lSize = m - l + 1;
		int rSize = r - m;
		
		int[] left = new int[lSize]; 
		int[] right = new int[rSize];
		
		for ( int i=0; i < lSize; ++i) {
			left[i] = array[l + i];
		}
		
		for ( int j=0; j < rSize; ++j){
			right[j] = array[m + 1 + j];
		}
		
		// Initial indexes of first and second sub-arrays
        int i = 0, j = 0;
 
        // Initial index of merged sub-array array
        int k = l;
        while (i < lSize && j < rSize)
        {
            if (left[i] <= right[j])
            {
                array[k] = left[i];
                i++;
            }
            else
            {
                array[k] = right[j];
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < lSize)
        {
            array[k] = left[i];
            i++;
            k++;
        }
 
        /* Copy remaining elements of R[] if any */
        while (j < rSize)
        {
            array[k] = right[j];
            j++;
            k++;
        }
		
	}
	
	public void sort ( int[] array, int l, int r) {
		if ( l < r ) {
			int mid = (l + r) / 2;
			sort( array, l, mid);
			sort( array, mid + 1, r);
			merge ( array, l, mid, r);
		}
	}
	
	public static void main(String[] args) {
		int[] arr = { 30, 15,26,10, 33, 20, 54};
		MergeSort ms = new MergeSort();
		ms.sort(arr, 0, arr.length -1);
		System.out.println("Array elements are:");

		for ( int i=0; i < arr.length; i++){
			System.out.print(arr[i] + ",");
		}
	}
	
}
