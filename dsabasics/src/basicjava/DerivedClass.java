package basicjava;

public class DerivedClass extends SuperClass {

	DerivedClass(int no) {
		super(no);
	}

	public static void main(String[] args){
		DerivedClass myObj = new DerivedClass(10);
		System.out.println(myObj.toString());
	}
}
