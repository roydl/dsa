package basicjava;

public class InitializationDemoClass {
	{
		System.out.println("Empty block");
	}
	
	static {
		System.out.println("Static block");
	}
	
	public static void main(String[] args) {
		InitializationDemoClass t = new InitializationDemoClass();
		InitializationDemoClass t1 = new InitializationDemoClass(); // note - static initializer will not be called
	}
}
