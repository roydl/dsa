package basicjava;

enum Day {
	MON, TUE, WED, THU,
	FRI, SAT, SUN
}

public class EnumDemo {
	Day day;
	
	public EnumDemo( Day day){
		this.day = day;
	}
	
	public void fortuneTeller (){
		switch (day){
		case MON:
		case TUE:
		case WED:
		case THU:
			System.out.println("Great day!");
			break;
		default:
			System.out.println("Superb day!");
			break;	
		}
	}
	
	public void main(String[] args) {
		EnumDemo ed = new EnumDemo(Day.FRI);
		ed.fortuneTeller();
	}
}
