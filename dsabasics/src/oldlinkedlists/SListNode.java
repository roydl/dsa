package oldlinkedlists;

public class SListNode {
	int _data;
	SListNode _next;
	
	public SListNode() {
		this(0);
	}
	
	public SListNode(int data) {
		this(data, null);
	}
	
	public SListNode(int data, SListNode next) {
		this._data = data;
		this._next = next;
	}
}