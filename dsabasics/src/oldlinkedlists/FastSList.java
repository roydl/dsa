package oldlinkedlists;

import java.util.Comparator;

public interface FastSList<E> {

	int size();
	
	int capacity();
	
	void add( E o );
	
	E get( int index );

	E set ( int index, E o );
	String toString();
	
	boolean remove( E o );
	
	void clear();

	void sort( Comparator<? super E> comparator);

}
