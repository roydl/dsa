package oldlinkedlists;

public class ListArrayImpl {
	int[] a;
	int lastPosition;
	final static int LIST_CAPACITY = 5;
	
	ListArrayImpl() {
		this(LIST_CAPACITY);
	}
	
	ListArrayImpl(int capacity){
		a = new int[capacity];
		lastPosition = -1;
	}
	
	void insertAt(int newItem, int loc){
		if ( ++lastPosition == a.length ){
			int[] b = new int[2 * a.length]; // create a new list
			for (int i=0; i < a.length; i++){
				b[i] = a[i];
			}
			a = b;
		}
		
		// shift every element to the right of position by one to create room
		for ( int j = lastPosition; j >= lastPosition; j--) {
			a[j+1] = a[j];
		}
		a[loc] = newItem;
	}
	
	void add(int newItem){
		if ( ++lastPosition == a.length ){ // resize automatically
			int[] b = new int[2 * a.length]; // create a new list
			for (int i=0; i < a.length; i++){
				b[i] = a[i];
			}
			a = b;
		}
		
		a[lastPosition] = newItem;
	}
	
	void printList() {
		System.out.println("The contents of the list are:");
		for (int k : a){
			System.out.print(k + ", ");
		}
		System.out.println("EOL");
	}

	public static void main(String[] args) {
		ListArrayImpl myList = new ListArrayImpl();
		myList.add(10);
		myList.add(30);
		myList.add(9);
		myList.add(8);
		myList.add(40);
		myList.printList();
		myList.insertAt(20, 1);
		myList.printList();
	}

}
