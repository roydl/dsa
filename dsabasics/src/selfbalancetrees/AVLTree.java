package selfbalancetrees;

class Node {
    int data;
    Node left, right;
    
    Node(int data) {
        this.data = data;
        left = null;
        right = null;
    }
    
    int getData() {
        return data;
    }
    
    Node getLeft() {
        return left;
    }
    
    Node getRight() {
        return right;
    }
    
    void setLeft(Node node) {
        left = node;
    }
    
    void setRight(Node node) {
        right = node;
    }
    
    void setData(int data) {
        this.data = data;
    }
}

public class AVLTree {
    private Node root;
    
    private Node insert( Node node, int data ) {
    	Node local = new Node(data);
        if ( node == null ) {
            node = local;
            return node;
        }
        
        if ( data < node.getData() ) {
            node.setLeft( insert(node.getLeft(), data));
            if ( balancedFactor(node) > 1 ) {
                System.out.println("Left of Left case");
                node = leftOfLeft(node);
            } 
            
            if ( balancedFactor(node) < -1 ) {
                System.out.println("Right of Left case");
                node = rightOfRight(node);
            }

            
        } else {
            node.setRight( insert(node.getRight(), data));
            if ( balancedFactor(node) < -1 ) {
                System.out.println("Right of Right case");
                node = rightOfRight(node);
            }
            
            if ( balancedFactor(node) > 1 ) {
                System.out.println("Left of Right case");
                node = leftOfLeft(node);
            }
        }
        return node;
    }
    
    public void insert( int data ) {
        root = insert(root, data);
    }
    
    public Node leftOfLeft(Node node) {
        Node lNode = node.left; //B
        Node tmp = lNode.right; //Q
       
        // rotate
        lNode.right = node; // B's right becomes A
        node.left = tmp; // A's left becomes Q
        return lNode;
    }
    
    public Node rightOfRight(Node node) {
        Node rNode = node.right;
        Node tmp = rNode.left;
        
        // rotate
        rNode.left = node;
        node.right = tmp;
        return rNode;
    }
    
    

    
    public void inOrderTraversal(Node node) {
        if ( node == null ) {
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.println(node.getData() + " Balanced Factor is: " + balancedFactor(node));
        inOrderTraversal(node.getRight());
    }
    
    // balanced factor = height of left tree - height of right tree
    public int balancedFactor (Node node) {
        if (node == null ) {
            return 0;
        }
        
        return height(node.left) - height(node.right);
    }
    
    public int height( Node node ) {
        if ( node == null ) {
            return 0;
        }
        
        int leftHeight = height(node.left);
        int rightHeight = height(node.right);
        
        return  (leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1);
    }
    
    public Node getRoot(){
        return root;
    }

	public void setup(int[] array) {
        for ( int i : array ) {
    	    insert(i);
        }
	}
	
    public static void main(String[] args) {
        AVLTree tree = new AVLTree();
        int[] array = {4,5,6,9,1,2,3,10,7,8,11,12};
        tree.setup(array);
        
        System.out.print("Recursive InOrderTraversal: ");
        tree.inOrderTraversal(tree.getRoot());
        System.out.println(" ");
    }    
}