package binarytrees;

import java.util.LinkedList;
import java.util.Queue;

public class BSTImpl {
	BinaryTreeNode _root;
	
	public BinaryTreeNode insert( BinaryTreeNode root, int data) {
		if ( root == null ) {
			try {
				root = new BinaryTreeNode(data);
			}
			catch (Exception e) {
				System.out.println("Memory error");
				e.printStackTrace();
				return null;
			}
			return root;
		}
		
		if ( data < root.data ) {
			root.left = insert(root.left, data);
		}
		else if ( data > root.data ){
			root.right = insert(root.right, data);
		}
		
		return root;
	}
	
	public BinaryTreeNode findMin( BinaryTreeNode root ) {
		if ( root == null ) {
			return null;
		}
		
		if ( root.left == null ) {
			return root;
		}
		
		else return findMin( root.left );
	}

	
	public BinaryTreeNode findMax( BinaryTreeNode root ) {
		if ( root == null ) {
			return null;
		}
		
		if ( root.right == null ) {
			return root;
		}
		
		else return findMax( root.right );
	}
	
	public boolean find( BinaryTreeNode root, int data){
		if ( root == null ){
			return false;
		}
		
		if ( data == root.data) {
			return true;
		}
		
		if ( data < root.data ) {
			return find( root.left, data );
		}
		
		if ( data > root.data ) {
			return find( root.right, data );
		}
		
		return false;
	}
	
	public BinaryTreeNode delete( BinaryTreeNode root, int data ){
		if ( root == null ) return null;
		BinaryTreeNode tmp;
		
		if ( data < root.data ) {
			root.left = delete( root.left, data );
		}
		else if ( data > root.data ) {
			root.right = delete( root.right, data );
		}
		else {
			if ( root.left == null ) {
				return root.right;
			}
			else if ( root.right == null ) {
				return root.left;
			}
			
			tmp = findMax(root.left);
			root.data = tmp.data;
			root.left = delete(root.left, root.data);
		}
		return root;
	}
	
	public BinaryTreeNode getRoot(){
		return _root;
	}
		
	public void levelOrderTraversal ( BinaryTreeNode root ) {
		if ( root == null ) {
			return;
		}
		
		Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
		queue.add( root );
		StringBuffer sb = new StringBuffer();
		sb.append('[');
		
		while ( ! queue.isEmpty() ) {
			BinaryTreeNode current = queue.peek();
			sb.append( current.data );
			sb.append( ',' );
			
			if ( current.left != null ) queue.add( current.left );
			if ( current.right != null ) queue.add( current.right );
			
			queue.remove(current);
		}
		
		sb.append( ']' );
		System.out.println( "Level order traversal: " + sb.toString());
	}

	public static void main(String[] args) {
		BSTImpl bst = new BSTImpl();
		bst._root = bst.insert(null, 6);
		bst.insert(bst.getRoot(), 2);
		bst.insert(bst.getRoot(), 1);
		bst.insert(bst.getRoot(), 4);
		bst.insert(bst.getRoot(), 8);
		bst.insert(bst.getRoot(), 5);
		bst.insert(bst.getRoot(), 7);
		bst.insert(bst.getRoot(), 3);

		System.out.println("Min element is: " + bst.findMin(bst.getRoot()).data);
		System.out.println("Max element is: " + bst.findMax(bst.getRoot()).data);
		
		System.out.println("Found 2 :" + bst.find(bst.getRoot(), 2));
		System.out.println("Found 20 :" + bst.find(bst.getRoot(), 20));
		System.out.println("Found 8 :" + bst.find(bst.getRoot(), 8));
		System.out.println("Found -2 :" + bst.find(bst.getRoot(), -2));
		bst.levelOrderTraversal(bst.getRoot());
		bst.delete(bst.getRoot(), 2);
		bst.levelOrderTraversal(bst.getRoot());


	}
}
