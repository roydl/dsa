package binarytrees;

public class BinaryTreeNode {
	Integer data;
	BinaryTreeNode left;
	BinaryTreeNode right;
	
	BinaryTreeNode( Integer data) {
		this.data = data;
		this.left = null;
		this.right = null;
	}
}
