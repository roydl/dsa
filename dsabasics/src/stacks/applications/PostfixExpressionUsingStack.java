package stacks.applications;

class IntStack {
    private int top = -1;
    private static int CAPACITY = 10;
    private int [] array;
    
    IntStack () {
        this(CAPACITY);        
    }
    
    IntStack ( int capacity ) {
        array = new int[capacity];
    }
    
    public void push( int item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public int pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        int tmp = array[top];
        top--;
        return tmp;
    }
    
    public int top() {
        return array[top];    
    }   
    
    public boolean isEmpty() {
        return top < 0;
    }
}

public class PostfixExpressionUsingStack {
    
 	private int compute( int op1, int op2, char operator) {
		switch ( operator ) {
		case '*':
			return op1 * op2;
		case '/':
			return op1 / op2;
		case '+':
			return op1 + op2;
		case '-':
			return op1 - op2;
		}
		return -1;
	}
	
	public int parsePostfixStringExpression (String in) throws Exception {
		IntStack stack = new IntStack();
		
		boolean isNumber = false;
		boolean isOperator = false;
		
		StringBuffer sb = new StringBuffer();
		for ( int i=0; i < in.length(); i++) {

			int tmp = 0;
			try {
			    switch (in.charAt(i)) {
			        case '+':
			        case '/':
			        case '-':
			        case '*':
			           isOperator = true;
			           isNumber = false;
			           break;
			        case '#': // number boundary
			            tmp = Integer.parseInt(sb.toString());
				        sb.delete(0, sb.length());
				        isNumber = true;
				        isOperator = false;
				        break;
                    default:
    			        sb.append(in.charAt(i));
	    		        isNumber = false;
		    	        isOperator = false;
		    	        break;
			    }
			}
			catch ( NumberFormatException e ){
				isOperator = true;
			}

			if ( isNumber ) {
				stack.push(tmp);
			}
			else if ( isOperator ) {
				int operand2 = stack.pop();
				int operand1 = stack.pop();
				int operand3 = compute (operand1, operand2, in.charAt(i)); // for debug reasons
				stack.push( operand3 );
			}
		}
		return stack.pop();
	}
	
	public static void main(String[] args) {
		String s = "130#42#510#*+";
		String s1 = "2#3#*4#5#6#/*-";

		PostfixExpressionUsingStack pf = new PostfixExpressionUsingStack();
		try {
    		System.out.println( "The evaluation of the PostFix expression " + s + " is " + pf.parsePostfixStringExpression(s));
		    System.out.println( "The evaluation of the PostFix expression " + s1 + " is " + pf.parsePostfixStringExpression(s1)); 
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}