package stacks.applications;

class MyStackImpl {
    private int top = -1;
    private static int CAPACITY = 10;
    private char [] array;
    
    MyStackImpl() {
        this(CAPACITY);        
    }
    
    MyStackImpl( int capacity ) {
        array = new char[capacity];
    }
    
    public void push( char item ) throws Exception {
        if ( top + 1 == array.length ) {
            throw new Exception("Stack is full");
        }
        
        array[++top] = item;
    }
    
    public char pop() throws Exception {
        if ( top == -1 ) {
           throw new Exception("Stack is empty"); 
        }
        
        char tmp = array[top];
        top--;
        return tmp;
    }
    
    public char top() {
        return array[top];    
    }   
    
    
    public boolean isEmpty() {
        return top < 0;
    }
}

public class InFixToPostFix {
    
 	private static int precedence ( char operator) {
		switch ( operator ) {
		case '(':
		    return 0;
		case ')':
		    return -1;
		case '^':
		    return 3;        
		case '*':
			return 2;
		case '/':
			return 2;
		case '+':
			return 1;
		case '-':
			return 1;
		}
		return -1;
	}
	
	public static boolean isOperator( char c) {
	    if ( c == '+' || c == '-' || c == '/' || c == '*' || c == '^' || c == '(' || c == ')' ) {
	        return true;
	    }
	    return false;
	}
	
	public static String inFixToPostFixExpression ( String in ) throws Exception {
	    MyStackImpl stack = new MyStackImpl();
	    StringBuffer sb = new StringBuffer();
	    
	    // iterate through the inbound expression
	    for ( int i = 0; i < in.length(); i++) {
	        char ch = in.charAt(i);
	        // check if the char is an operator
            if ( isOperator( ch ) ){
                if ( ch == '(') {
                    stack.push(ch);
                }
                // if current operator precedence is gt stack top -- push on the stack
                else if ( stack.isEmpty() || (precedence(ch) > precedence(stack.top())) ) {
                    stack.push(ch);
                } else {
                    // if current operator precedence <= stack top -- pop
                    while ( ! stack.isEmpty() && (precedence(ch) <= precedence(stack.top()))) {
                        // special handling for parenthesis
                        if ( ch == ')' && stack.top() == '(') {
                            stack.pop();
                            break;
                        }
                        else {
                            sb.append(stack.pop());
                        }
                    }
                    
                    // ensure closing brace is not pushed on the stack
                    if ( ch != ')')
                        stack.push(ch);
                }
            }
            else {
                // non operators are sent to post fix operator
                sb.append(ch);
            }
        }
        
        // once parsing is complete - rest of the stack should be popped and appended to the postfix string
        while (! stack.isEmpty()) {
            sb.append( stack.pop() );
        }        
        return sb.toString();
	}
	
	public static void main(String[] args) {
	    String s1 = "a*b+c";
	    String s2 = "a+b*c";
	    String s3 = "a+b*c*d*e";
	    String s4 = "a+b*c^d";
	    String s5 = "a*b^c+d";
	    String s6 = "a+(b*c-d)-(e*f)";
	    String s7 = "(a*b-c)+(d*e-f)*(g+h-k)";
	    
	    try {
	        System.out.println("Infix operator " + s1 + " converted to postFix is: " + inFixToPostFixExpression(s1));
	        System.out.println("Infix operator " + s2 + " converted to postFix is: " + inFixToPostFixExpression(s2));
		    System.out.println("Infix operator " + s3 + " converted to postFix is: " + inFixToPostFixExpression(s3));
		    System.out.println("Infix operator " + s4 + " converted to postFix is: " + inFixToPostFixExpression(s4));
		    System.out.println("Infix operator " + s5 + " converted to postFix is: " + inFixToPostFixExpression(s5));
		    System.out.println("Infix operator " + s6 + " converted to postFix is: " + inFixToPostFixExpression(s6));
		    System.out.println("Infix operator " + s7 + " converted to postFix is: " + inFixToPostFixExpression(s7));

	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}