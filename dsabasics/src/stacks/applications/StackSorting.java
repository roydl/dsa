package stacks.applications;

/*
 * Return max value from the stack using only stack functions pop or push
 * Do not consider it as an array
 * Use recursion as well as non recursive methods
 * o(n) is both cases are quadratic
 */
public class StackSorting {
	
	public static IntStack sort( IntStack iStack ) throws Exception {
		if ( iStack.isEmpty() ) return iStack;
		
		IntStack oStack = new IntStack();
		while ( ! iStack.isEmpty() ) {
			int item = iStack.pop();
		
			while (! oStack.isEmpty() && item < oStack.top()) {
				iStack.push(oStack.pop());
			}
			
			oStack.push(item);
		}
		
		return oStack;
	}
	
	public static int max ( IntStack iStack ) throws Exception {
		IntStack oStack = sort( iStack );
		if ( oStack.isEmpty() ) {
			return Integer.MIN_VALUE;
		}
		
		return oStack.top();
	}
	
	public static void insert ( IntStack iStack, int x ) throws Exception {
		if ( ! iStack.isEmpty() ) {
			int y = iStack.top();
			if ( x < y) {
				iStack.pop();
				insert( iStack, x );
				iStack.push(y);
			} 
			else {
				iStack.push(x);
			}
		}
		else {
			iStack.push(x);
		}
	}
	
	public static void stackSort( IntStack iStack ) throws Exception {
		if ( ! iStack.isEmpty() ) {
			int tmp = iStack.pop();
			stackSort( iStack );
			insert( iStack, tmp );
		}
	}
	
	public static void main(String[] args) {
		try {
			IntStack stack = new IntStack();
			stack.push(20);
			stack.push(12);
			stack.push(11);
			stack.push(4);
			stack.push(9);
			
			stackSort(stack);
			while ( !stack.isEmpty() ) {
				System.out.print(stack.pop() + ", ");
			}
			System.out.println();
			System.out.println("Max stack element is:" + max(stack));

			stack.push(20);
			stack.push(12);
			stack.push(11);
			stack.push(4);
			stack.push(9);
			
			System.out.println("Max stack element is:" + max(stack));
		}
		catch ( Exception e) {
			
		}
	}
}
