package stacks.applications;

import stacks.BasicStack;

public class ReverseArray {
	

	public static void main(String[] args){
		int[] array = { 2, 3, 4, 5, 6};
		
		BasicStack bs = new BasicStack(array.length);
		for (int i : array) {
			bs.push((int)i);
		}
		
		System.out.println("Size of the array is: " + array.length + " & size of stack: " + bs.size());

		int[] reversedArray = new int[5];
		for (int k = 0; k < reversedArray.length; k++){
			reversedArray[k] = (int)bs.pop();
		}
		
		System.out.println("Array elements printed below:");
		for (int j : reversedArray) {
			System.out.print(j + " , ");
		}
	}
}
