package stacks;

public class FullStackException extends RuntimeException {
	private static final long serialVersionUID = 176453723L;

	public FullStackException( String err ) {
		super(err);
	}
}
